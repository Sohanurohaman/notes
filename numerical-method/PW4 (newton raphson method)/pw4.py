def find_fx(a):
    t = a * a + 4 * a - 5
    return t
def find_f1x(a):
    t = 2 * a - 4
    return t
x0 = 1 
tol = 0.001 
N = 10
NA = 999
cur_x = x0
prev_x = NA
i = 0
while i <= N:
    Fx = find_fx(cur_x)
    F1x = find_f1x(cur_x)
    if prev_x == NA: 
        prev_x = cur_x
        next_x = cur_x - (Fx / F1x)
        cur_x = next_x
    else:
        diff = abs(cur_x - prev_x)
        if diff <= tol:
            print("The Root is:", cur_x)
            break
        else:
            if F1x == 0:
                print("Derivative is zero. Newton-Raphson method fails.")
                break
            prev_x = cur_x
            next_x = cur_x - (Fx / F1x) 
            cur_x = next_x
    i += 1
    dadas 