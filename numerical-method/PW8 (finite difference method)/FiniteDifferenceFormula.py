def f(x):
    return x**2 + 2*x + 1
x = 2
h = 0.1
forward_diff = (f(x + h) - f(x)) / h
backward_diff = (f(x) - f(x - h)) / h
central_diff = (f(x + h) - f(x - h)) / (2 * h)
exact_derivative = 2 * x + 2
print(f"Forward Difference: {forward_diff:.4f}")
print(f"Backward Difference: {backward_diff:.4f}")
print(f"Central Difference: {central_diff:.4f}")
print(f"Exact Derivative: {exact_derivative:.4f}")
