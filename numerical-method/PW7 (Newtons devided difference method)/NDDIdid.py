def newton_interpolation(x, y, value):
    # First divided differences
    dd1_0 = (y[1] - y[0]) / (x[1] - x[0])
    dd1_1 = (y[2] - y[1]) / (x[2] - x[1])
    # Second divided difference
    dd2 = (dd1_1 - dd1_0) / (x[2] - x[0])
    # Newton's interpolating polynomial P2(x)
    result = y[0] + (value - x[0]) * dd1_0 + (value - x[0]) * (value - x[1]) * dd2
    return result
x = [1, 2, 3]
y = [1, 4, 9]
value = 2.5
y_interpolated = newton_interpolation(x, y, value)
print(f"y({value}) = {y_interpolated:.4f}")  # Output: y(2.5) = 6.2500
