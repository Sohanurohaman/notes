def find_P2X(a, b0, b1, p0, c1, c2):
    t1 = p0
    t2 = (a - b0) * c1
    t3 = (a - b0) * (a - b1) * c2
    return t1 + t2 + t3
# main part
x = [1, 2, 3] # x_data
y = [1, 4, 9] # y_data
#find dd1
dd1 = []
i = 0 
while i < 2:
    tn = y[i+1] - y[i]
    td = x[i+1] - x[i]
    tm = tn/td
    dd1.append(tm)
    i = i + 1
#find dd2
dd2 = (dd1[1] - dd1[0]) / (x[2] - x[0])
# for x = 2.5 find y(x)
tx = 2.5
ty = find_P2X(tx, x[0], x[1], y[0], dd1[0], dd2)
print("y(x) = {}".format(ty))
