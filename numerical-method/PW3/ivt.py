def find_f(x):
    return x**3 - 3*x + 1

def find_sub_intervals(a, b, h):
    intervals = []
    x = a
    while x < b:
        f_x1 = find_f(x)
        f_x2 = find_f(x + h)
        if f_x1 * f_x2 < 0: 
            intervals.append((x, x + h))
        x += h 
    return intervals 

a = -2
b = 4
h = 1 

sub_intervals = find_sub_intervals(a, b, h)

print("Sub intervals:", sub_intervals)

