import math
def integer(num) :
    if num > 0:
        return "Positive"
    if num < 0:
        return "Negative"
    else:
        return "zero"
test_values = [0, -5, 10]
results = {num: integer(num) for num in test_values}
print(results)