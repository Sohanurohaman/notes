def find_fx(a):
    t = a * a * a - a - 1
    return t
def find_next_x(a, b):
    f_a = find_fx(a)
    f_b = find_fx(b)
    if f_b - f_a == 0:
        raise ValueError("Division by zero encountered in secant method.")
    return (b -((b - a) / (f_b - f_a))*f_b)
x0 = 1
x1 = 2
N = 15
prev_x = x0
cur_x = x1
next_x = 0
y_list = []
# Initial function values
y0 = find_fx(prev_x)
y1 = find_fx(cur_x)
y_list.append(y0)
y_list.append(y1)
i = 2
while i <= N:
    next_x = find_next_x(prev_x, cur_x)
    next_y = find_fx(next_x)
    y_list.append(next_y)
    # Check if the root is found
    if abs(cur_x - next_x) < 1e-6 :
        print(f"Root found: x = {cur_x:4f}")
        break
    else:
        prev_x = cur_x
        cur_x = next_x
        i += 1