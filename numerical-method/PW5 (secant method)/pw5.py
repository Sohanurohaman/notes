# def find_fx(a):
#     t=a*a-3
#     return t
# def find_next_x (a,b):
#     t1=(b-a)/(find_fx(b)-find_fx(a))
#     t2 = b-find_fx(b)*t1
#     return t2

#  x0=1, x1=2, N=15
# prev_x=x0, cur_x=x, next_x=0
# y_list = []
# y0=find_fx(prev_x)
# y1=find_fx(cur_x)
# y_list.append(y0)
# y_list.append(y1)

# i=2
# while i<= N:
#     next_x = find_next_x(prev_x,cur_x)
#     next_y = find_fx(next_x)
#     y_list.append(next_x)
#     if cur_x == next_x and find_fx(cur_x)=0:
#         print("x=", cur_x)

#     else:
#         prev_x=cur_x
#         cur_x=next_x
#         i=i+1


def find_fx(a):
    t = a * a - 3
    return t

def find_next_x(a, b):
    t1 = (b - a) / (find_fx(b) - find_fx(a))
    t2 = b - find_fx(b) * t1
    return t2

# Initial values
x0 = 1
x1 = 2
N = 15
prev_x = x0
cur_x = x1
next_x = 0
y_list = []

# Initial function values
y0 = find_fx(prev_x)
y1 = find_fx(cur_x)
y_list.append(y0)
y_list.append(y1)

i = 2
while i <= N:
    next_x = find_next_x(prev_x, cur_x)
    next_y = find_fx(next_x)
    y_list.append(next_y)
    
    # Check if the root is found
    if abs(cur_x - next_x) < 1e-6 and abs(find_fx(cur_x)) < 1e-6:
        print("Root found: x =", cur_x)
        break
    else:
        prev_x = cur_x
        cur_x = next_x
        i += 1
else:
    print("Did not converge within", N, "iterations.")

# Output results
print("y_list:", y_list)
